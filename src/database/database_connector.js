const mongoose = require('mongoose')
const colors = require('colors')

const { db } = require('../../config.dev')

module.exports = {
    connectDB: connectDB = async () => {
        try {
            const connect = await mongoose.connect(db(process.env.M_DB_USER, process.env.M_DB_PASSWORD, process.env.M_DB_NAME), {
                useUnifiedTopology: true,
                useNewUrlParser: true
            })
            console.log(colors.bgGreen('DB Connected ', connect.connection.host))
        } catch (error) {
            console.log(colors.bgRed('Error found while connecting DB ', error.message))
        }
    }
}