// /**
//  * Common function to call GET, POST methods, (This is not correct way for huge app, but still tried)
//  * 
//  */
const { Propertity, User } = require('../models/model')
const QueriesUtils = require('../../utils/queries.utils')
const { uploadToCloudinary } = require('../../utils/cloudinary.utils')

const mongoose = require('mongoose')
class PropertiesApiHelper {
    constructor() {
        this.endPoint = ''
    }

    async call(req, res) {

        /**
         * example of endpoint: http://localhost:5000/api/properties?page=0&limit=5&filter=kr
         */

        const { q, page, limit, filter, googleId, userId, propertyId } = req.query

        const user = await User.findOne({ _id: userId })
        const totelProperty = await Propertity.find({})

        let favorites = ''
        if (user) favorites = user.activities.favourites

        const { doc } = new QueriesUtils(Propertity)
            .findAll()
            .findOne({ propertyId })
            .sortBy()
            .populate({ googleId })
            .populateFav({ favorites })
            .pagination({ page, limit })
        // .search({ searchBy: 'title', q })
        // .filter({ filterBy: 'contact.address.area', filter })
        const properties = await doc.select('-__v -createdAt -updatedAt')

        res.send({
            totalCount: totelProperty.length,
            currentCount: properties.length,
            properties
        })

    }

    async add(req, res) {
        const {
            propertyTitle,
            propertyPrice,
            sqFt,
            propertyType,
            propertyConfiguration,
            propertyContact,
            propertyArea,
            propertyPincode,
            googleId
        } = req.body

        const user = await User.findOne({ googleId })

        if (!user) {
            return res.status(401).send({
                message: 'Logged in user only allowed add property, \nPlease login via your google accout. \nGoogle will provide us only your email and name.'
            })
        }
        const uploadedImages = await uploadToCloudinary(req);

        const contact = { propertyContact, address: { propertyArea, propertyPincode } }
        const images = { thumbnail: uploadedImages[0].secure_url, large: uploadedImages[1].secure_url }

        const newProperty = new Propertity({
            propertyTitle,
            propertyPrice,
            sqFt,
            propertyType,
            propertyConfiguration,
            contact,
            images,
            googleId,
            user: user._id
        })

        await newProperty.save();
        res.send({
            data: newProperty
        })

    }

}
module.exports = new PropertiesApiHelper()