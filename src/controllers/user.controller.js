const { User } = require('../models/model')

const loginUser = async (req, res) => {

    const { name, email, googleId, imageUrl } = req.body

    const isUserExist = await User.findOne({ googleId })
    if (isUserExist) {
        console.log(isUserExist)
        return res.send({ googleId: isUserExist.googleId, dbId: isUserExist._id })
    }

    const addUser = new User({
        name, email, googleId, imageUrl
    })

    await addUser.save()

    const getNewUser = await User.findOne({ email })

    res.status(200).send({ googleId: getNewUser.googleId, dbId: getNewUser._id })

};

const getSingleUser = async (req, res) => {

    const { params: { googleId } } = req

    const user = await User.findOne({ googleId }).select('-_id, -__v')

    if (!user) {
        return res.status(404).send({ message: 'User not found' })
    }

    res.status(200).send({ user })

};

const updateActivities = async (req, res) => {

    const { id, propertyId } = req.body

    // add $pull method for remove
    const addtoFav = await User.findByIdAndUpdate({ _id: id }, {
        $push: {
            'activities.favourites': propertyId
        }
    }, {
        new: true,
        useFindAndModify: false
    })
    res.status(200).send({ addtoFav })

};


module.exports = { loginUser, getSingleUser, updateActivities }