const { Propertity } = require('../models/model')
const { uploadToCloudinary } = require('../../utils/cloudinary.utils');

const postProperty = async (req, res) => {
    const {
        propertyTitle,
        propertyPrice,
        sqFt,
        propertyType,
        propertyConfiguration,
        propertyContact,
        propertyArea,
        propertyPincode,
    } = req.body

    const uploadedImages = await uploadToCloudinary(req);

    const contact = { propertyContact, email, address: { propertyArea, propertyPincode } }
    const images = { thumbnail: uploadedImages[0].secure_url, large: uploadedImages[1].secure_url }

    const newProperty = new Propertity({
        propertyTitle,
        propertyPrice,
        sqFt,
        propertyType,
        propertyConfiguration,
        propertyContact,
        propertyArea,
        propertyPincode,
        images
    })

    console.log(newProperty)
    // const newProps = await newProperty.save()

    res.status(201).json({
        message: 'Sucessfully posted',
        newProps
    })
}

module.exports = postProperty