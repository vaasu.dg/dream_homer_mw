const mongoose = require('mongoose')

module.exports = {
    userSchema: {
        name: { type: String, trim: true },
        email: { type: String, required: true, trim: true },
        googleId: { type: String },
        imageUrl: { type: String }, // Need to check if image got updated --- (FUTURE SPRINT)
        activities: {
            history: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Propertity' }],
            favourites: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Propertity' }],
            purchased: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Propertity' }],
            sold: [{
                properties: { type: mongoose.Schema.Types.ObjectId, ref: 'Propertity' },
                soldDate: { type: Date, default: Date.now() }
            }]
        },
        properties: [
            { type: mongoose.Schema.Types.ObjectId, ref: 'Propertity' }
        ],
        registeredDate: { type: Date, default: Date.now() }
    }
}