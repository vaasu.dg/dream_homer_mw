module.exports = {
    adminSchema: {
        name: { type: String },
        email: { type: String, unique: true },
        password: { type: String, required: true }
    }
}