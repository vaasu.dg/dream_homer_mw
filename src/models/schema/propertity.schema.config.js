const mongoose = require('mongoose')

module.exports = {
    propertiySchema: {
        propertyTitle: { type: String, required: true },
        propertyPrice: { type: String, required: true },
        sqFt: { type: String, required: true },
        propertyType: {
            type: [String],
            required: true,
            enum: {
                values: ['Residential Apartment', 'Independent/Builder Floor', 'Independent House/Villa', 'Others']
            }
        },
        propertyConfiguration: {
            type: [String],
            required: true,
            enum: {
                values: ['1 bhk', '2 bhk', '3 bhk', '4 bhk', '4+ bhk']
            }
        },
        contact: {
            propertyContact: { type: String, required: true },
            address: {
                propertyArea: { type: String, required: true, text: true },
                propertyPincode: { type: String, required: true }
            }
        },
        isSold: { type: Boolean, default: false },
        images: {
            thumbnail: { type: String, required: true },
            large: { type: String, required: true }
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        googleId: { type: String, required: true }
    }
}