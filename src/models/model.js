const mongoose = require('mongoose')
const Schema = mongoose.Schema

const { propertiySchema } = require('./schema/propertity.schema.config')
const { userSchema } = require('./schema/user.schema.config')
const { adminSchema } = require('./schema/admin.schema.config')


const PropertiySchema = new Schema(propertiySchema, { timestamps: true })
const UserSchema = new Schema(userSchema)
const AdminSchema = new Schema(adminSchema)

const Propertity = mongoose.model(`properties`, PropertiySchema)
const User = mongoose.model(`user`, UserSchema)
const Admin = mongoose.model(`admin`, AdminSchema)

module.exports = { Propertity, User, Admin }
