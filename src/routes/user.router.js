const router = require('express').Router()

const { loginUser, getSingleUser, updateActivities } = require('../controllers/user.controller')

router
    .get('/:googleId', (req, res) => getSingleUser(req, res))
    .post('/', (req, res) => loginUser(req, res))
    .patch('/', (req, res) => updateActivities(req, res))

module.exports = router