const router = require('express').Router()
const PropertiesApiHelper = require('../controllers/propertity.controller')
const { fileUpload } = require('../../utils/cloudinary.utils');

const propertiesApiGenerator = (req, res) => PropertiesApiHelper.call(req, res)
const propertyAdd = (req, res) => PropertiesApiHelper.add(req, res)

router
    .get('/list', (req, res) => propertiesApiGenerator(req, res))
    // .post('/add', (req, res) => propertyAdd(req, res))
    .post('/add', fileUpload, (req, res) => propertyAdd(req, res))
module.exports = router