const router = require('express').Router()

const postProperty = require('../controllers/dashboard.controller')
const { fileUpload } = require('../../utils/cloudinary.utils');

// Add middleware to store in session - (Future)
function isAdmin(req, res, next) {
    req.admin = { adminUser: process.env.ADMIN_USER, adminPassword: process.env.ADMIN_PASSWORD }
    next()
}

router
    .get('/', ({ res }) => res.render('src/pages/dashboard', { title: 'Dashboard' }))
    .post('/', fileUpload, async (req, resp) => postProperty(req, resp))

module.exports = router