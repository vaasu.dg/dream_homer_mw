
module.exports = {
    db: (u, p, n) => `mongodb://${u}:${p}@ds253353.mlab.com:53353/${n}`,
    cloudinaryOptions: (n, k, s) => ({
        cloud_name: n,
        api_key: k,
        api_secret: s
    })
}