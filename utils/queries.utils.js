
class QueriesUtils {

    constructor(doc) {
        this.doc = doc
    }

    findAll() {
        this.doc = this.doc.find({})
        return this
    }

    findOne({ propertyId }) {
        if (!propertyId) return this.findAll()
        this.doc = this.doc.findOne({ _id: propertyId })
        return this
    }

    sortBy() {
        this.doc = this.doc.sort({ 'createdAt': -1 })
        return this
    }

    pagination({ page = 0, limit = 10 }) {
        this.doc = this.doc.skip(page * limit).limit(limit * 1)
        return this
    }

    search({ searchBy = '', q }) {
        if (!searchBy) return this.findAll()
        this.doc = this.doc.find({ [searchBy]: new RegExp(q, 'i') })
        return this
    }

    filter({ filterBy = '', filter }) {
        if (!filterBy) return this.findAll()
        this.doc = this.doc.find({ [filterBy]: new RegExp(filter, 'i') })
        return this
    }

    populate({ googleId }) {
        if (!googleId) return this.findAll()
        this.doc = this.doc.find({ googleId })
        return this
    }

    populateFav({ favorites }) {
        if (!favorites) return this.findAll()
        this.doc = this.doc.find({ _id: favorites })
        return this
    }


}

module.exports = QueriesUtils