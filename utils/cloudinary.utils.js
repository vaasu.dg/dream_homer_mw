const path = require('path')
const Datauri = require('datauri')
const multer = require('multer')
const cloudinary = require('cloudinary').v2
const { cloudinaryOptions } = require('../config.dev')

/***
 * Taken referance for memory storage
 * https://medium.com/@joeokpus/uploading-images-to-cloudinary-using-multer-and-expressjs-f0b9a4e14c54
 * 
 */
// Multer Setup for memory based storage
const storage = multer.memoryStorage()
const fileUpload = multer({ storage }).single('thumbnail')

// Converting Base64 to string
const dUri = new Datauri()
const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer)

// Cloudinary configuration
cloudinary.config(cloudinaryOptions(process.env.C_NAME, process.env.C_API_KEY, process.env.C_API_SECRET))

const imageUploadByRes = async (req, resolution) =>
    await cloudinary.uploader.upload(dataUri(req).content, { public_id: `homer_images/${Date.now()}_${resolution}_${req.file.originalname}`, width: `${resolution}` })


const uploadToCloudinary = async function (req) {
    const imgLowRes = await imageUploadByRes(req, 200)
    const imgHighRes = await imageUploadByRes(req, '1.4')
    return [imgLowRes, imgHighRes]
}

module.exports = {
    uploadToCloudinary,
    fileUpload
}