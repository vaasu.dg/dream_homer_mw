const path = require('path')
const express = require('express')
const cors = require('cors')

const { connectDB } = require('./src/database/database_connector')

const app = express()


if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({ path: path.resolve(__dirname, './.env.local') })
}

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: path.resolve(__dirname, './.env') })
}

app.use([
    express.json(),
    express.urlencoded({ extended: true }),
    cors(),
    express.static(path.join(__dirname, './static/views'))
])

app.use((error, req, res, next) => {
    if (res.headersSent) {
        return next(err)
    }
    res.status(500).send('INTERNAL SERVER ERROR !')
})

app.set('views', path.join(__dirname, './src/static/views'))
app.set('view engine', 'ejs')

console.clear()

const dev = require('./config.dev')

app.use('/dashboard', require('./src/routes/dashboard.router'))
app.use('/user', require('./src/routes/user.router'))

// Public Endpoints
app.use('/api/property', require('./src/routes/propertity.router'))

app.listen(process.env.PORT, _ => {
    connectDB()
    console.log(`App starter @ http://localhost:${process.env.PORT}`)
})

